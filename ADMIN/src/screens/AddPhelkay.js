import { StyleSheet, Text,Image,Keyboard, View,ScrollView, TouchableOpacity} from 'react-native'
import React, { useState } from 'react';
  import { Dropdown } from 'react-native-element-dropdown';
  import AntDesign from 'react-native-vector-icons/AntDesign';
  import UploadImage from '../../Components/Image.js';
  import firebase from '../Firebase/config.js'
  import { SafeAreaView } from 'react-native-safe-area-context';
  import ButtonOne from '../../Components/ButtonOne';
  import { Ionicons } from '@expo/vector-icons';
  import { TextInput } from 'react-native-paper';



  const AddPhelkay  = ({navigation, route}) => {
    const Name='item-'+ new Date().toISOString();
    const item = route.params
    const [value, setValue] = useState(null);
    const [tittle, setTittle] = useState('');
    const [process, setProcess] = useState('');
    const [ingredient, setIngredient] = useState('');
    const [lunch, setLunch] = useState([]);
    
    const AddLunch = () => {
      if (tittle && tittle.length > 0) {
        const timestamp = firebase.firestore.FieldValue.serverTimestamp();
        console.log('working add breakfast')
          const data = {
              top: tittle,
              steps:process,
              needs:ingredient,
              createdAt: timestamp
          };
          
          firebase.firestore().collection('lunch').doc(item.id)
              .update(data)
              .then(() => {
                  Keyboard.dismiss();
                  navigation.navigate('HomeScreen') 
              })
              .catch((error) => {
                  alert(error);
              })
              alert('Successfully added')
            }
          }
    
    return (
          
      <SafeAreaView style={{maxheight:'100%', backgroundColor:'white'}}>
          <ScrollView>
              <View>    
                   <View style={{backgroundColor:'#2f3d4f', flexDirection:'row',alignItems:'center'}}>
                       <View style={{marginTop:20, flexDirection:'row',marginLeft:20}}>
                        <TouchableOpacity onPress={()=>navigation.replace("HomeScreen")}>
                        <Ionicons name="md-chevron-back-outline" size={24} color="white"  /> 
                        </TouchableOpacity>
                        <Text style={{marginLeft:120,marginBottom:15, fontSize:20,color:'white'}}>Edit</Text>
                     </View>
                  </View>
                    
                      <View  style={{width:800,height:800}}>
                        
                      <View style={styles.h}>
                      <View style={styles.view}>
                            <Text>Phelkay</Text>
                            </View >
                        <View style={styles.input}>
                        <TextInput
                                  numberOfLines={1}
                                  multiline={true}
                                  onChangeText={(top) => setTittle(top)}
                                      value={tittle}
                                    placeholder={item.top}  
                                  style={{
                                      width:275, 
                                      borderWidth:1,
                                      borderColor:'black',
                                      backgroundColor:'white',
                                      marginLeft:20,
                                      marginBottom:5,
                                      textAlignVertical: "top",
                                     
                                }}
                              
                              />
                        </View>
                             
                      </View>
                       
                            
                        
                          
                        
                      
                      <View style={{flexDirection:'row'}}>
                          
                      
                      </View>
                      <View></View>
                      <View style={styles.view}>
                            <Text>Zheysa</Text>
                       </View>
                       <View style={styles.input}>
                       <TextInput 
                                numberOfLines={5}
                                multiline={true}
                                onChangeText={(steps) => setProcess(steps)}
                                      value={process}
                                      placeholder={item.steps}
                                style={{
                                  width:275,
                                  borderWidth:1,
                                  borderColor:'black',
                                  backgroundColor:'white',
                                  marginLeft:20,
                                  textAlignVertical: "top",
                              }}
                              
                              /> 
                       </View>
                             
                          
                     
                      <View style={styles.view}>
                            <Text>Examples </Text>
                      </View>
                      <View style={styles.input}>
                      <TextInput
                                    numberOfLines={5}
                                    multiline={true}
                                    onChangeText={(needs) => setIngredient(needs)}
                                      value={ingredient}
                                    placeholder={item.needs}
                                      
                                      
                                    style={{
                                        width:275,
                                        borderWidth:1,
                                        borderColor:'black',
                                        backgroundColor:'white',
                                        margin:10,
                                        marginLeft:20,
                                        textAlignVertical: "top",
                                      
                                        
                                        
                                        
                                    }}
                              />
                              
                          
                      </View>
                      {/* <View style={styles.view}>
                        <TouchableOpacity onPress={()=>navigation.replace("Pick")}>
                         <Text>Add Image</Text>
                        </TouchableOpacity>
                            
                       </View> */}
                             
                          
                     
                      <View style={styles.drop}>
                      
                          
                          {/* <UploadImage></UploadImage> */}
                          
                      </View >
                      <View  style={{flexDirection:'row',}}>
                        <ButtonOne  onPress={()=>navigation.navigate("HomeScreen")}> CANCEL  </ButtonOne>
                        <ButtonOne  onPress={AddLunch} >ADD </ButtonOne>
                        </View>
                      </View>
              </View>
              <View style={{height:500}}>
  
              </View>
              </ScrollView>
              </SafeAreaView>
            )
          }
          
  
  const styles = StyleSheet.create({
      view:{
          flexDirection:'row',
          marginTop:20,
          marginLeft:35,
          
          
  
      },
      input:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:20,
        
      },
      drop:{
        flexDirection:'column'  
      },
      dropdown: {
        margin: 16,
        height: 50,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
      
      },
      icon: {
        marginRight: 5,
      },
        placeholderStyle: {
        fontSize: 16,
      },
      selectedTextStyle: {
        fontSize: 16,
      },
      iconStyle: {
        width: 20,
        height: 20,
      },
      inputSearchStyle: {
        height: 40,
        fontSize: 16,
      },
      
      
  })
  export default AddPhelkay
  