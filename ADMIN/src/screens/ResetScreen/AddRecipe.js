import { StyleSheet, Text,Image,Keyboard, View,TextInput,Button,TouchableOpacity} from 'react-native'
import React, { useState } from 'react';
  import { Dropdown } from 'react-native-element-dropdown';
  import AntDesign from 'react-native-vector-icons/AntDesign';
  import UploadImage from '../../../Components/Image';
  import firebase from '../../Firebase/config.js'
  const data = [
    { label: 'Breakfast', value: '1' },
    { label: 'Lunch', value: '2' },
    { label: 'Dinner', value: '3' },
    { label: 'Snacks', value: '4' },
    { label: 'Drinks', value: '5' },
  ];


const AddRecipe = ({navigation}) => {
  const [value, setValue] = useState(null);
  // recipes collection reference
  const [tittle, setTittle] = useState('');
  const [process, setProcess] = useState('');
  const [ingredients, setIngredients] = useState('');
  const [category, setCategory] = useState('');
  const tittleRef = firebase.firestore().collection('recipes');
  const processRef = firebase.firestore().collection('recipes');
  const ingredientsRef = firebase.firestore().collection('recipes');
  const categoryRef = firebase.firestore().collection('recipes'); 
  const [recipes, setRecipes] = useState([]); 


  
    const addRecipe = () => {
      // check if we have a todo.
      if (tittle && tittle.length > 0) {
          // get the timestamp
          const timestamp = firebase.firestore.FieldValue.serverTimestamp();
          // structure the data  to save
          const data = {
              text: tittle,
              createdAt: timestamp
          };
          // add the data to firestore db
          tittleRef
              .add(data)
              .then(() => {
                  // release todo state
                  setTittle('');
                  // release keyboard
                  Keyboard.dismiss();
              })
              .catch((error) => {
                  // show an alert in case of error
                  alert(error);
              })
              alert('Successfully added')

      if (process && process.length > 0) {
                // get the timestamp
                const timestamp = firebase.firestore.FieldValue.serverTimestamp();
                // structure the data  to save
                const data = {
                    steps:process,
                    createdAt: timestamp
                };
                // add the data to firestore db
                processRef
                    .add(data)
                    .then(() => {
                        // release todo state
                        setProcess('');
                        // release keyboard
                        Keyboard.dismiss();
                    })
                    .catch((error) => {
                        // show an alert in case of error
                        alert(error);
                    })
                    alert('Successfully added')
       if (ingredients && ingredients.length > 0) {
                      // get the timestamp
                      const timestamp = firebase.firestore.FieldValue.serverTimestamp();
                      // structure the data  to save
                      const data = {
                          steps:ingredients,
                          createdAt: timestamp
                      };
                      // add the data to firestore db
                      ingredientsRef
                          .add(data)
                          .then(() => {
                              // release todo state
                              setIngredients('');
                              // release keyboard
                              Keyboard.dismiss();
                          })
                          .catch((error) => {
                              // show an alert in case of error
                              alert(error);
                          })
                          alert('Successfully added')
        if (category && category.length > 0) {
                            // get the timestamp
                            const timestamp = firebase.firestore.FieldValue.serverTimestamp();
                            // structure the data  to save
                            const data = {
                                steps:category,
                                createdAt: timestamp
                            };
                            // add the data to firestore db
                            categoryRef
                                .add(data)
                                .then(() => {
                                    // release todo state
                                    setCategory('');
                                    // release keyboard
                                    Keyboard.dismiss();
                                })
                                .catch((error) => {
                                    // show an alert in case of error
                                    alert(error);
                                })
                                alert('Successfully added')
      }
    }
  }
      

    }
  }
    
  return (
    <View>
            <TouchableOpacity  onPress={()=>navigation.replace("AddPage")}>
              <Image  style={{width:30,height:30,marginTop:40,marginLeft:30}} source={require('../../../assets/back.png')}></Image>
            </TouchableOpacity>
            <View  style={{width:800,height:800}}>
              <Text style={{marginLeft:10,marginTop:40,color:'blue'}} >ADD RECIPES</Text>
              <View style={styles.view}>
                  <Text>Tittle</Text>
                    <TextInput
                        numberOfLines={1}
                        multiline={true}
                        onChangeText={(text) => setTittle(text)}
                            value={tittle}
                        style={{
                            width:150,
                            height:30,
                            borderWidth:1,
                            borderColor:'black',
                            backgroundColor:'white',
                            marginLeft:25,
                            
                      }}
                    
                    />
                   
              
                
              </View >
            
            <View style={{flexDirection:'row'}}>
                <UploadImage></UploadImage>
            
            </View>
            
            <View style={styles.view}>
                  <Text>process</Text>
                    <TextInput
                      numberOfLines={5}
                      multiline={true}
                      onChangeText={(text) => setProcess(text)}
                            value={process}
                      style={{
                        width:150,
                        height:100,
                        borderWidth:1,
                        borderColor:'black',
                        backgroundColor:'white',
                        marginLeft:20,
                    }}
                    
                    /> 
                
                
            </View>
            <View style={styles.view}>
                  <Text>ingredients </Text>
                    <TextInput
                          numberOfLines={5}
                          multiline={true}
                          onChangeText={(text) => setIngredients(text)}
                            value={ingredients}
                          style={{
                              width:100,
                              height:100,
                              borderWidth:1,
                              borderColor:'black',
                              backgroundColor:'white',
                              marginLeft:5,
                          }}
                    />
                
                
            </View>
            <View style={styles.view}>
            
                <Text>Category
                onChangeText={(text) => setCategory(text)}
                        value={category}
                
                  <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        inputSearchStyle={styles.inputSearchStyle}
                        iconStyle={styles.iconStyle}
                        data={data}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder="Select category"
                        searchPlaceholder="Search..."
                        value={value}
                        onChange={item => {
                          setValue(item.value);
                          
                        }}
                       
                        renderLeftIcon={() => (
                        <AntDesign style={styles.icon} color="black" name="Safety" size={20} />
                        )}
                        
                  />
              

        
                </Text>
                
                
            </View>
            <View style={{backgroundColor:'blue',width:'5%',height:'5%',marginLeft:300,marginTop:30,alignItems:'center',justifyContent:'center'}}>
                  <TouchableOpacity onPress={addRecipe}>
                    <Text style={{ color:'white',}}>ADD</Text>
                  </TouchableOpacity>
            </View>
                
            
          

          
        </View>
    </View>
  )
}


const styles = StyleSheet.create({
    view:{
        flexDirection:'row',
        marginTop:30,
        marginLeft:50,

    },
    dropdown: {
      margin: 16,
      height: 50,
      borderBottomColor: 'gray',
      borderBottomWidth: 0.5,
    },
    icon: {
      marginRight: 5,
    },
      placeholderStyle: {
      fontSize: 16,
    },
    selectedTextStyle: {
      fontSize: 16,
    },
    iconStyle: {
      width: 20,
      height: 20,
    },
    inputSearchStyle: {
      height: 40,
      fontSize: 16,
    },
    
    
})
export default AddRecipe
