import React, { useState } from "react";
import Button from "../../../Components/Button";
import Header from "../../../Components/Header";
import Background from "../../../Components/Background";
import TextInput from "../../../Components/TextInput";
import BackButton from "../../../Components/BackButton";
import { passwordValidator } from '../../core/helpers/passwordValidator';
import { confirmpasswordValidator } from '../../core/helpers/confirmpasswordValidator';

export default function ConfirmPasswordScreen({navigation}) {
    const [password, setPassword] = useState({ value: "", error: ""})
    const [confirmpassword, setConfirmPassword] = useState({ value: "", error: ""})

    const onConfirmPressed = () => {
        const passwordError = passwordValidator(password.value);
        const confirmpasswordError = confirmpasswordValidator(confirmpassword.value);
        if (passwordError || confirmpasswordError) {
            setPassword({ ...password, error: passwordError });
            setConfirmPassword({ ...confirmpassword, error: confirmpasswordError });
        }
        else {
            navigation.navigate('LoginScreen') 
        }
    }
    const onCancelPressed = () => {
        navigation.navigate('LoginScreen')
    }
    return (
        <Background>
            <BackButton goBack={navigation.goBack} />
            <Header>Reset Password</Header>
            <TextInput 
                label="New Password" 
                value={password.value}
                error={password.error}
                errorText={password.error}
                onChangeText={(text) => setPassword({ value: text, error: ""})}
                secureTextEntry
            />
            <TextInput 
                label="Confirm new password" 
                value={confirmpassword.value}
                error={confirmpassword.error}
                errorText={confirmpassword.error}
                onChangeText={(text) => setConfirmPassword({ value: text, error: ""})}
                secureTextEntry
            />
            <Button mode="contained" onPress = {onConfirmPressed}>Confirm</Button> 
            <Button mode="contained" onPress = {onCancelPressed}>Cancel</Button>
        </Background>
    )
}
