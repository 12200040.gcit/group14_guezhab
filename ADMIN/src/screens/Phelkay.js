import React,{useState,useEffect} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View,TextInput, ScrollView,SafeAreaView, FlatList } from "react-native";
import firebase from '../Firebase/config';
import { FontAwesome } from "@expo/vector-icons";
import { Ionicons } from '@expo/vector-icons';
import ButtonOne from '../../Components/ButtonOne';
import { FontAwesome5 } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons'; 
import { AntDesign } from '@expo/vector-icons'; 

export default function Phelkay ({navigation}) {
    const [lunch, setLunch] = useState([]);
    const tittleRef = firebase.firestore().collection('lunch'); 
    
    useEffect(() => {
  
      tittleRef
          .orderBy('createdAt', 'desc')
          .onSnapshot(
              querySnapshot => {
                  const newLunch = []
                  querySnapshot.forEach((doc)=>{
                    const { top,needs,steps, downloadURL} = doc.data()
  
                    newLunch.push({
                      id: doc.id,
                      top,
                      needs,
                      steps,                  
                      downloadURL,
  
                    })
                  })
                  setLunch(newLunch)
              },
              error => {
  
                  console.error(error);
              }
          )
  }, []);
  const deleteTittle = (tittle) => {
    tittleRef	
        .doc(tittle.id)
        .delete()
        .then(() => {
            alert("Deleted successfully");
        })
        .catch(error => {
            alert(error);
        })
  }

  
  const renderTittle = ({ item }) => {
      <View style={styles.tittleContainer} >
              
                <Text style={styles.tittleText}>
                    {item.text[0].toUpperCase() + item.text.slice(1)}
                </Text>
            
  
        </View>
    return (
  
        <View style={styles.tittleContainer} >
              
                <Text style={styles.tittleText}>
                    {item.text[0].toUpperCase() + item.text.slice(1)}
                </Text>
            
  
        </View>
    )
  }
  return (
  
    <SafeAreaView style={{backgroundColor:'#EEECED'}}>
        
        <View style={{backgroundColor:'#2f3d4f', }}>
       
        <View style={{marginTop:25,}}>
          <View style={{marginTop:10,flexDirection:'row',marginLeft:20}} >
          <TouchableOpacity onPress={()=>navigation.replace("HomeScreen")}>
             
             <Ionicons name="md-chevron-back-outline" size={24} color="white" /> 
             </TouchableOpacity>
          <Text style={{marginLeft:110,marginBottom:15, fontSize:20,color:'white'}}>Phelkay</Text>
          </View>
       
        </View>
        
          </View>
          
        
      
      <View style={{height:'100%'}}>
  
      {lunch.length > 0 && (
                <View style={styles.listContainer}>
                    <FlatList
                        data={lunch}
                        
                        renderItem={({ item }) => {
                          return(
                               
                              <View key={item.id} style={styles.f}>
                                
                                <Image  style={{width:30,height:30,marginLeft:15,width:100,height:50,borderRadius:10,marginTop:20}} source={{uri: item.downloadURL}} /> 
                                    
                               
                                  <Text style={{ color:'white',marginLeft:50,fontSize:20,marginTop:30}}>{item.top}</Text>
                                  
                                    <View  style={{marginLeft:40,marginTop:0,flexDirection:'row'}}>
                                    <View>
                                
                              <View style={{marginLeft:10,
                                            marginTop:30}} 
                                            >
                                <FontAwesome name="trash-o" size={20} color="red" onPress={() => deleteTittle(item)}  />      
                             
                                </View>
                        
                         </View>
                      
                    <TouchableOpacity onPress={()=>navigation.replace("ViewItem1",item)}>
                    <View style={{marginLeft:20,
                                            marginTop:30}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                     </View>
                    </TouchableOpacity>
                             
                
                    <TouchableOpacity onPress={()=>navigation.replace("AddLunch1",item)}>
                              <View style={{marginLeft:25,
                                            marginTop:30}}      >
                              <AntDesign name="edit" size={24} color="green" />
                                </View>
                    </TouchableOpacity>
                  </View>
                                   
                                   <Text>{'\n'}</Text> 
                                  
                                     
                                    
                                    </View>
                                                 
                                
                                
                                    
                           )
                         }} />
                  <View style={{height:200}}>
                                </View>
                </View>
                 
            )}
      </View>
    </SafeAreaView>
  )
  }
  
  const styles = StyleSheet.create({
    container: {
     marginTop: 20,
    },
    
      
    
    add:{
      width: 60,
      height: 60,
      marginLeft:300,
      marginTop:100,
    },
    icon:{
      color:'white',
      height:60,
      justifyContent:'center',
      alignItems:'center',
      marginLeft:170,
     paddingTop:20,
     fontSize:20,
    },
    tittleContainer: {
      borderBottomColor: 'black',
      borderWidth: 1,
      flex:1 ,
      flexDirection: 'column',
      height: 100,
      width: 100,
      margin: 5,
      backgroundColor: 'blue'
    },
    tittleText: {
      fontSize: 16,
      color: 'white',
      textAlign: 'center'
    },
    listContainer: {
      height: '100%',
      marginTop:10,
      
      
    },
    place:{
          
    },
    f:{
      backgroundColor:'#2f3d4f',
      height: 100,
      width:400,
      marginLeft:10,
      borderTopEndRadius:10,
      borderTopStartRadius:10,
      borderBottomEndRadius:10,
      borderBottomStartRadius:10,
      marginBottom:10,
      flexDirection:'row'
      
  
    },
    g:{
      backgroundColor:'#ADD8E6',
      height:150,
      width: 250,
      borderTopEndRadius:20,
      borderTopStartRadius:20,
      borderBottomEndRadius:10,
      borderBottomStartRadius:10,
      marginTop:10
      
    },
    
    
  });
  
  
  
  