import React,{useState,useEffect} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView, FlatList } from "react-native";
// import Action from "../../component/Components/Action";
// import TextInput from "../../component/Components/TextInput";
// import {firebase} from '../Firebase/config';
// import Icon from 'react-native-vector-icons/Ionicons';
// import { FontAwesome5 } from '@expo/vector-icons';
// import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons'; 
export default function ViewItem ({navigation,route}) {
  const item = route.params;
  console.log(item); 
  

return (

  <SafeAreaView style={{backgroundColor:'#EEECED'}}>
      
      <View style={{backgroundColor:'#2f3d4f', }}>
     
      <View style={{marginTop:25,}}>
        <View style={{marginTop:10,flexDirection:'row',marginLeft:20}} >
        <TouchableOpacity onPress={()=>navigation.replace("Zheysa")}>
           
           <Ionicons name="md-chevron-back-outline" size={24} color="white" /> 
           </TouchableOpacity>
        <Text style={{marginLeft:110,marginBottom:15, fontSize:20,color:'white'}}>Zheysa</Text>
        </View>
     
      </View>
      
        </View>
        <ScrollView>
        <View key={item.id} style={styles.f}>
              
              <Image  style={{marginLeft:16,width:320,height:170,borderRadius:10,marginTop:20,color:'white'}} source={{uri: item.downloadURL}} />        
          <Text style={{marginLeft:20,marginTop:20,fontWeight:'bold',fontSize:20,color:'white'}}>zheysa</Text>
              <Text style={{marginLeft:20,marginTop:10,fontSize:17,color:'white'}}> {item.top}</Text>


              <Text style={{marginLeft:20,marginTop:20,fontWeight:'bold',fontSize:20,color:'white'}}>Phelkay</Text>
              <Text style={{marginLeft:20,marginTop:10,fontSize:17,color:'white'}}> {item.steps}</Text>
          <View  style={{marginLeft:0,marginTop:5,}}>
                <Text style={{marginLeft:20,marginTop:10,fontWeight:'bold',fontSize:20,color:'white'}}>Examples:</Text>
                <Text style={{marginLeft:20,marginTop:10,fontSize:17,color:'white'}}> {item.needs}</Text>
          </View> 
          {/* <View  style={{marginLeft:0,marginTop:5,}}>
                <Text style={{marginLeft:20,marginTop:10,fontWeight:'bold',fontSize:20}}>Process</Text>
                <Text style={{marginLeft:20,marginTop:10,fontSize:17}}> {item.ok}</Text>
          </View>   */}
          
           
          
        </View> 
              <View style={{height:200}}>
                </View>         
        </ScrollView>
             
                                  
      
  </SafeAreaView>

)
}
const styles = StyleSheet.create({
  container: {
   marginTop: 10,
  },
  // image: {
  //   flex: 1,
  //   justifyContent: "center",
  //   width: '95%',
  //   height: 200,
  //   marginLeft:20,
  //   overflow:'hidden',
  
  // text: {
  //   color: "white",
  //   fontSize: 28,
  //   lineHeight: 84,
  //   fontWeight: "bold",
  //   marginTop:130
    
  
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  tittleContainer: {
    borderBottomColor: 'black',
    borderWidth: 1,
    flex:1 ,
    flexDirection: 'column',
    height: 100,
    width: 100,
    margin: 5,
    backgroundColor: 'blue'
  },
  tittleText: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  listContainer: {
    height: '100%',
    marginTop:40,
    
    
  },
  place:{
        
  },
  f:{
    backgroundColor:'#2f3d4f',
    height: 600,
    width: 375,
    marginLeft:20,
    borderTopEndRadius:10,
    borderTopStartRadius:10,
    borderBottomEndRadius:20,
    borderBottomStartRadius:20,
    marginTop:10
    

  },
  g:{
    backgroundColor:'#ADD8E6',
    height:915,
    width: 350,
    borderTopEndRadius:20,
    borderTopStartRadius:20,
    borderBottomEndRadius:20,
    borderBottomStartRadius:20,
    marginTop:10
    
  }
  
});



