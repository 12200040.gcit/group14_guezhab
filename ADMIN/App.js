import React from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import RegistrationScreen from "./src/screens/RegistrationScreen/RegistrationScreen";
import LoginScreen from "./src/screens/LoginScreen/LoginScreen";
import { HomePage } from "./src/screens/HomeScreen/HomePage";
import ResetPasswordScreen from "./src/screens/RegistrationScreen/ResetPasswordScreen";
import {  ResetScreen, HomeScreen } from "./src/screens";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from './Components/DrawerContent';
import LogOut from './src/screens/ResetScreen/LogOut';
import AddZheysaContent from './src/screens/AddZheysaContent';
import AddZheysa from './src/screens/AddZheysa';
import AddPhelkayContent from './src/screens/AddPhelkayContent';
import Category from './src/screens/Category';
import Zheysa from './src/screens/Zheysa'
import ViewItem from './src/screens/ViewItem'
import Phelkay from './src/screens/Phelkay'
import ViewItem1 from './src/screens/ViewItem1'
import AddPhelkay from './src/screens/AddPhelkay'
import Pick from './src/screens/Pick'
import Pick1 from './src/screens/Pick1'

const Stack = createNativeStackNavigator();
const Drawer =createDrawerNavigator();


export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='HomeScreen'
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="LoginScreen" component={LoginScreen} />      
        <Stack.Screen name="RegistrationScreen" component={RegistrationScreen} />
        <Stack.Screen name="ResetPasswordScreen" component={ResetPasswordScreen} /> 
        <Stack.Screen name="ResetScreen" component={ResetScreen} />
        <Stack.Screen name="HomeScreen" component={DrawerNavigator} />
        <Stack.Screen name="adminLogout" component={LogOut}/> 
        <Stack.Screen name="AddZheysaContent" component={AddZheysaContent} />
        <Stack.Screen name="AddZheysa" component={AddZheysa} />
        <Stack.Screen name="AddPhelkayContent" component={AddPhelkayContent} />
        <Stack.Screen name="Category" component={Category} />
        <Stack.Screen name="Zheysa" component={Zheysa} />
        <Stack.Screen name="ViewItem" component={ViewItem} />
        <Stack.Screen name="Phelkay" component={Phelkay} />
        <Stack.Screen name="ViewItem1" component={ViewItem1} />
        <Stack.Screen name="AddPhelkay" component={AddPhelkay} />
        <Stack.Screen name="Pick" component={Pick} />
        <Stack.Screen name="Pick1" component={Pick1} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
const DrawerNavigator =() =>{
  return(
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='HomeScreen' component={HomeScreen}/>
    </Drawer.Navigator>
  )
}


 