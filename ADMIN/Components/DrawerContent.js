import { StyleSheet, Text, View,Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';


 const DrawerContent = ({navigation}) => {
  return (
     <View >
    <View style={{backgroundColor:'white',width:'70%',marginTop:50, marginLeft:40}}>
       <TouchableOpacity>
        <Image style={styles.admin}
          source={require('../assets/admin.png')}>
        </Image>  
       </TouchableOpacity>
       <View style={{flexDirection:'row',marginTop:30}}>
      <TouchableOpacity style={{flexDirection:'row'}}>
        <Image style={styles.home}
          source={require('../assets/home.png')}>
       </Image>
        <Text style={{marginLeft:20, color:'black'}}>HOME</Text>
      </TouchableOpacity>
     </View>
       <View>
      {/* <TouchableOpacity style={{flexDirection:'row'}}>
        <Image style={styles.update}
          source={require('../assets/add.png')}>
        </Image>
        <Text style={{marginLeft:20,color:'black'}} onPress={()=>navigation.replace("Category")}>ADD RECIPES</Text>
      </TouchableOpacity> */}
      </View>
      <View>
      {/* <TouchableOpacity style={{flexDirection:'row'}}>
        <Image style={styles.update}
          source={require('../assets/update.png')}>
        </Image>
        <Text style={{marginLeft:20,color:'black'}} onPress={()=>navigation.replace("adminUpdate")}>UPDATE PROFILE</Text>
      </TouchableOpacity> */}
      </View>
      <View style={{flexDirection:'row'}}>
      <TouchableOpacity style={{flexDirection:'row'}}>
        <Image style={styles.logout}
          source={require('../assets/logout.png')}>
        </Image>
        <Text style={{marginLeft:20,color:'black'}} onPress={()=>navigation.replace("LoginScreen")}>LOGOUT</Text>
      </TouchableOpacity>
      </View> 
      </View>
     
    </View>
  )
}

export default DrawerContent;

const styles = StyleSheet.create({
  admin: {
    width:200,
    height:200,
    marginTop:45,
    marginLeft:10,
    borderRadius:100
  },
  home: {
    width:30,
    height:30,
    marginBottom:30,
    marginLeft:50

  },
  update: {
    width:30,
    height:30,
    marginBottom:30,
    marginLeft:50
  },
  logout: {
    width:30,
    height:30,
    marginBottom:30,
    marginLeft:50
  }
})