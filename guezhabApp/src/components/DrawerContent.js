import React from "react"
import {View,Text,StyleSheet,BackHandler,TouchableOpacity, Alert} from 'react-native'
import { DrawerItem,DrawerContentScrollView } from "@react-navigation/drawer"
import { Avatar, Title,Drawer } from "react-native-paper"
import { AntDesign } from '@expo/vector-icons';
import { render } from "react-dom";
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';


export default function DrawerContent({navigation}){
  
    return(
    <View style={{flex:1}}>
       <DrawerContentScrollView style={{backgroundColor:'#46caeb'}}>
           {/* <View style={styles.drawerContent}> */}
               <View style={styles.head}>
                   <Avatar.Image
                   style={{backgroundColor:"white",resizeMode:'contain'}}
                    size={80}
                    source={
                        require('../../assets/backgroundimage.png')
                    }
                   />
                   <Title style={styles.title}>Guezhab</Title>
                </View>

                   <View style={styles.userInfoSection}>
                   <Drawer.Section style={styles.drawerSection}>
                       {/* <DrawerItem
                        label="History" icon={({})=> <MaterialIcons name="history" size={24} color="black" />}
                       /> */}
                            <DrawerItem
                        label="About Us"
                        onPress={()=>navigation.navigate("Aboutus")}
                    icon={({})=><MaterialCommunityIcons name="account-details-outline" size={24} color="black" />}
                       />
                       <TouchableOpacity
            onPress={() =>
              Alert.alert(
                'འཐོན་སྒོ།',
                'འཐོན་འགྱོ་ནི་ཨིན?',
                [
                  {text: 'ཨིན།', onPress:()=>BackHandler.exitApp()},
                  {text: 'མེན།', onPress: () => console.log('Canceled'), style: 'cancel'},
                ],
                {
                  cancelable: true
                }
            )}
            style={{marginLeft:20}}
            >
                <View style={{flexDirection:'row',marginRight:170,justifyContent:'space-around',marginTop:30}}>
                     <MaterialIcons name="logout" size={24} color="black" />
                     <Text>འཐོན་སྒོ།</Text>
                </View>
            
            </TouchableOpacity>








                       
                           {/* <DrawerItem
                        onPress={navigation.replace()}
                        label="འཐོན་སྒོ།" 
                        icon={({})=> <MaterialIcons name="logout" size={24} color="black" />}

                       /> */}
                       {/* <AntDesign name="logout" size={24} color="black" /> */}
                      
                   </Drawer.Section>

               </View>

           {/* </View> */}
       </DrawerContentScrollView>
       </View>
    )
}

const styles = StyleSheet.create({
    head:{
        backgroundColor:'#46caeb',
        justifyContent:'center',
        alignItems:'center',
        // marginTop:30, 
        // height:200
       
    },
    // drawerContent:{
    //     flex:1
    // },
    userInfoSection:{
        paddingLeft:20,
        backgroundColor:'#46caeb',
        height:'100%'
    },
    title:{
        marginTop:20,
        fontWeight:'bold',
    },
    drawerSection:{
        justifyContent:'space-around'
    }
})


// import { StyleSheet, Text, View } from 'react-native'
// import React, { useEffect, useState } from 'react'
// import { SafeAreaView } from 'react-native-safe-area-context';
// import { ActivityIndicator } from 'react-native-paper';
// import { FlatList } from 'react-native-gesture-handler';

// const api_url = 'https://doctorapplicaton.herokuapp.com/api/Patient/'

// const App = () => {

//   const [isLoading, setLoading] = useState(true);
//   const [data, setData] = useState([]);

//   useEffect(()=>{
//     fetch(api_url)
//       .then((response) => response.json())
//       .then((json)=>setData(json.Patient))
//       .catch((error)=>alert(error))
//       .finally(()=> setLoading(false));
//   })


 
//   return (
//     <SafeAreaView style={{justifyContent:'center',alignItems:'center',flex:1}}>
//       {isLoading ? (
//         <ActivityIndicator/>
//       ):(
//         <View>
//          <FlatList
//           data={data}
//           keyExtractor={({id},index) => id}
//           renderItem={({item})=>(
//             <Text>
//               {item[0]}
//             </Text>
//           )}
//         /> 
//         </View>
        
//       )}
      
    
//     </SafeAreaView>
//   );

//   };
// export default App

// const styles = StyleSheet.create({})