import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native'

export default function PrimaryButton({children,onPress}) {
  return (
    <ScrollView>
    <TouchableOpacity onPress={onPress}>
       <View style={styles.container}>
         <Text> {children} </Text>
         </View>
    </TouchableOpacity>
    </ScrollView>


  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 60,
        width: 300,
        justifyContent: 'center',
        borderWidth: 1,
        margin: 7,
        padding: 6
        
    }
})