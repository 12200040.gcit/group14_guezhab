import firebase  from 'firebase/compat'
import 'firebase/compat/storage'
import firestore  from 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyDu3KN4clbPTCtRiyMAfE6S7gydUMl6Rjo",
    authDomain: "guezhab-1066f.firebaseapp.com",
    databaseURL: "https://guezhab-1066f-default-rtdb.firebaseio.com",
    projectId: "guezhab-1066f",
    storageBucket: "guezhab-1066f.appspot.com",
    messagingSenderId: "92164004934",
    appId: "1:92164004934:web:fb8aab45dffa2b801cfe47"
  };
  
  firebase.initializeApp(firebaseConfig);
  firebase.firestore();

  const storage =  firebase.storage();

  export {storage, firebase as default}
