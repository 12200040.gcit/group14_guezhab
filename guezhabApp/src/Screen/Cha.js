import React,{useState,useEffect} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView, FlatList } from "react-native";
import firebase from '../Firebase/config';
import Icon from 'react-native-vector-icons/Ionicons';
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
export default function BreakfastItem ({navigation,route}) {
  const item = route.params;
  console.log(item);
return (
  <SafeAreaView style={{backgroundColor:'#EEECED'}}>
      <View style={{backgroundColor:'#46caeb', width:"100%" }}>
     <View style={{marginTop:30,}}>
       <View style={{marginTop:15,flexDirection:'row',marginLeft:'5%'}} >
       <TouchableOpacity onPress={()=>navigation.goBack()}>
          <Ionicons name="md-chevron-back-outline" size={24} color="black" />
          </TouchableOpacity>
       <Text style={{marginLeft:130,marginBottom:15, fontSize:20,color:'black'}}>ཞེས།</Text>
       </View>
     </View>
       </View>
        <ScrollView>
        <Image  style={{marginLeft:16,width:'80%',height:170,borderRadius:10,marginTop:40,resizeMode:'contain'}} source={{uri: item.downloadURL}} />

        <View style={{flexDirection:'row',justifyContent:'space-around'}}>
       
      <View style={{marginVertical:20, marginTop:70}}>
        <Text style={{fontSize:23,fontWeight:'bold'}}>ཞེ་ས།</Text>
      </View>

      <View style={{marginVertical:20,marginTop:70}}>
        <Text style={{fontSize:23,fontWeight:'bold'}} >ཕལ་སྐད།</Text>
      </View>

      </View>
    
      <View style={{flexDirection:'row',justifyContent:'space-around'}}>


      <View style={{marginVertical:10,paddingHorizontal:10}}>
        <Text style={{fontSize:23}}>{item.top}</Text>
      </View>

      <View style={{marginVertical:10,paddingHorizontal:25}}>
       <Image
      // style={styles.imageBack}
      style={{width:25,height:25}}
      source={require('../../assets/revere.png')}
      /> 
      </View>

      <View style={{marginVertical:10,paddingHorizontal:15}}>
        <Text style={{fontSize:23}}>{item.steps}།</Text>
      </View>

      {/* <TouchableOpacity
      onPress={playSound}
     
      style={{marginVertical:14,paddingHorizontal:15}}>
         <Image
      style={{width:25,height:25}}
      source={require('../../assets/audio.png')}
      />
      </TouchableOpacity>

      */}

     

     
      </View>
{/* example */}

<View style={{marginTop:20,marginLeft:30,marginTop:50}}>
      <View style={{justifyContent:'center'}}>
        <Text style={{fontSize:20,marginLeft:15,marginTop:20,fontWeight:'bold'}}>བརྗོད་པ་དང་ལག་ལན་འཐབ་ཐངས།</Text>

      </View>

      <View>
        <Text style={{fontSize:20,marginLeft:15,marginTop:20}}>དཔེར་ན།</Text>
      </View>

      <View>
        <Text style={{fontSize:20,marginLeft:15,marginTop:20}}>{item.needs}</Text>
      </View>

      
</View>

              <View style={{height:200}}>
                </View>
        </ScrollView>
  </SafeAreaView>
)
}
const styles = StyleSheet.create({
  safeView:{
    backgroundColor:"#46caeb",
    flex:1
  },
  headView:{
    marginTop: 35,
  },
  videos:{
    width:'100%',
    height:300,
  },
  imageBack: {
    width: 25,
    height: 25,
    marginRight:25,
    marginLeft:10,
    marginTop:10
  },
  
})