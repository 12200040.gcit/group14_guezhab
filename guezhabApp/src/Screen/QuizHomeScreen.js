import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View, SafeAreaView, Image, TouchableOpacity,FlatList,Button } from 'react-native'
import { COLORS } from '../components/COLORS';
import { Ionicons } from '@expo/vector-icons';


const Quiz = ({navigation}) => {
  return (
    
      <View style={{flex:1}}>
        

      <View style={styles.head}>
      <TouchableOpacity onPress={()=>navigation.goBack()} style={{marginTop:35,marginLeft:5}}>
          <Ionicons name="md-chevron-back-outline" size={28} color="black" />
          </TouchableOpacity>
    
        <Text style={styles.Header}>གུས་ཞབས།</Text>
        </View>
    <View style={styles.container}>
    <Text style={{fontWeight:'bold',fontSize:40,textAlign:'center',alignItems:'center', marginBottom:30}}>རིག་བསྡུར།</Text>
      <TouchableOpacity 
      onPress={() => {
        navigation.navigate('colorquize')
      }}
      
      style={styles.content}>
        <Image
        style={styles.imageIc}
        source={require('../../assets/respect.png')}

        />
        <Text style={{textAlign:'center',fontWeight:'bold',fontSize:20,padding:10}}>ཞེ་ས་རིག་བསྡུར།</Text>
      </TouchableOpacity>
        
      <TouchableOpacity  
  
    onPress={() => {
     navigation.navigate('animalquize')
   }}
      style={styles.content}>
        <Image
        style={styles.imageIc}
        source={require('../../assets/friendship.png')}
        />
        <Text style={{textAlign:'center',fontWeight:'bold',fontSize:20,padding:10}}>ཕལ་སྐད་རིག་བསྡུར།</Text>
      </TouchableOpacity>

    
    </View>
 
    </View>
     
  )
}

export default Quiz

  const styles = StyleSheet.create({
    head:{
      flex:1,
      backgroundColor:'#46caeb',
      height:'100%',
    },
    Header:{
      fontSize:40,
      paddingLeft:120,
      fontWeight:'bold',
      marginTop:50
      
      
    },
   
  
      container:{
          flex:1,
          justifyContent:'center',
          alignItems:'center',
          // marginBottom:180,
          marginTop:-500,
          backgroundColor:'white',
          // height:'100%'
          borderTopRightRadius:50,
          borderTopLeftRadius:50,
          
      },
      content:{
        borderWidth:2,
        borderColor:'black',
        marginVertical:20,
        borderRadius:10,
        backgroundColor:'#46caeb',
        
      },
      imageIc:{
      width:250,
      height:100,
      resizeMode:'contain',
      opacity:0.98
      }
  })

