// import { StyleSheet, Text, View,TouchableOpacity,Image } from 'react-native'
// import React from 'react'

// const Aboutus = () => {
//   return (
//     <View style={styles.container}>
//          <View style={styles.headView}>
//         <TouchableOpacity onPress={()=>navigation.goBack("d")}>
//           <Image
//             style={styles.imageBack}
//             source={require('../../assets/back.png')} />
//         </TouchableOpacity>
//       </View>
//         <View style={{borderRadius:5,borderColor:'black',borderWidth:1,marginBottom:100}}>
//             <Text>About APP</Text>
//             <Text >lorem
//                  dsfjdslkfajjjjjjjj dsf df f dsjkflsd f  sfjl   jsdlf fsd fsdkjslfkd  slkdfflksdf   lskjdfdsf
//                  slkdfflksdfsdfs
//                  slkdfflksdfsdfs
//                  sdfasf
//                  slkdfflksdfsdfs
//                  sdfdsf
//                  dfsdf
//             </Text>
//         </View>

//         <View style={{borderRadius:5,borderColor:'black',borderWidth:1,marginBottom:100}}>
//         <Text>Mentor</Text>
//             <Text >lorem
//                  dsfjdslkfajjjjjjjj dsf df f dsjkflsd f  sfjl   jsdlf fsd fsdkjslfkd  slkdfflksdf   lskjdfdsf
//                  slkdfflksdfsdfs
//                  slkdfflksdfsdfs
//                  sdfasf
//                  slkdfflksdfsdfs
//                  sdfdsf
//             </Text>
//         </View>

//         <View style={{borderRadius:5,borderColor:'black',borderWidth:1,margin:0}}>
//         <Text>About APP</Text>
//             <Text >lorem
//                  dsfjdslkfajjjjjjjj dsf df f dsjkflsd f  sfjl   jsdlf fsd fsdkjslfkd  slkdfflksdf   lskjdfdsf
//                  slkdfflksdfsdfs
//                  slkdfflksdfsdfs
//                  sdfasf
//                  slkdfflksdfsdfs
//                  sdfdsf
//             </Text>
//         </View>
      
//     </View>
//   )
// }

// export default Aboutus

// const styles = StyleSheet.create({
//     container:{
//         flex:1,
//         justifyContent:'center',
//         alignItems:'center'
//     },
//     headView:{
//         marginTop: 35,
//       },
//       videos:{
//         width:'100%',
//         height:300,
//       },
//       imageBack: {
//         width: 25,
//         height: 25,
//         marginRight:25,
//         marginLeft:10,
//         marginTop:10
//       },
   
// })

import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View, SafeAreaView, Image, TouchableOpacity,Button } from 'react-native'
import firebase from '../Firebase/config';
import {getDownloadURL,ref, getStorage} from 'firebase/storage'
import PlaySound from '../playsound/playSound';
import { Audio } from 'expo-av';
import { ScrollView } from 'react-native-gesture-handler';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default function Cha ({ navigation }) {
 
  return (
   
    <View style={{flex:1}}>
<ScrollView>
    
      <View style={styles.headView}>
        <TouchableOpacity onPress={()=>navigation.goBack("d")}>
          <Image
            style={styles.imageBack}
            source={require('../../assets/back.png')} />
        </TouchableOpacity>
      </View>

    

      <View style={styles.first}>
        <Text style={{fontSize:20,fontWeight:'bold',paddingBottom:3}}>About App</Text>
        <Text style={{fontSize:15,}}>Guezhap App is platform where users can search for specific Zheysa and Phelkay words with some examples when and where to use these words.</Text>

      </View>


      <View style={styles.second}>
        <Text style={{fontSize:20,fontWeight:'bold',paddingBottom:3}}>Developers</Text>
        <Text style={{fontSize:15,}}>Absar Gurung </Text>
        <Text style={{fontSize:15,}}><MaterialCommunityIcons name="gmail" size={20} color="black" /> absargrg10@gmail.com {'\n'}</Text>
        <Text style={{fontSize:15,}}>Chencho Tshering</Text>
        <Text style={{fontSize:15,}}><MaterialCommunityIcons name="gmail" size={20} color="black" /> chenchozeo@gmail.com {'\n'}</Text>
        <Text style={{fontSize:15,}}>Kunzang Dolkar</Text>
        <Text style={{fontSize:15,}}><MaterialCommunityIcons name="gmail" size={20} color="black" /> Kdolkar123@gmail.com {'\n'}</Text>
        <Text style={{fontSize:15,}}>Tshewang Norbu</Text>
        <Text style={{fontSize:15,}}><MaterialCommunityIcons name="gmail" size={20} color="black" /> Tshewang58@gmail.com</Text>



      </View>
      <View style={styles.third}>
        <Text style={{fontSize:20,fontWeight:'bold', paddingBottom:3,}}>Mentor</Text>
        <Text style={{fontSize:15,}}>Ms.Sonam Wangmo</Text>
        <Text style={{fontSize:15,}}><MaterialCommunityIcons name="gmail" size={20} color="black" /> Sonamwangmo@gcit.edu.bt</Text>

      </View>


      </ScrollView>
   
    </View>
  
  )
}


const styles = StyleSheet.create({
  safeView:{
    backgroundColor:"white",
    flex:1
  },
  first:{
    backgroundColor:'#46caeb',
    flex:1,
    margin:30,
    marginBottom:0,
    elevation:15,
    borderRadius:20,
    padding:20
    

  },
  second:{
    backgroundColor:'#46caeb',
    flex:1,
    margin:30,
    marginBottom:0,
    elevation:15,
    borderRadius:20,
    padding:20
  },
  third:{
    backgroundColor:'#46caeb',
    flex:1,
    margin:30,
    marginBottom:0,
    elevation:15,
    borderRadius:20,
    padding:20,
    marginBottom:30
  },
  headView:{
    marginTop: 20,
  },
  videos:{
    width:'100%',
    height:300,
  },
  imageBack: {
    width: 25,
    height: 25,
    marginRight:25,
    marginLeft:10,
    marginTop:10
  },
  
})