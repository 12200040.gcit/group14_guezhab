import { StyleSheet, Text, View,Image,ScrollView} from 'react-native'
import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Zheysa from './Zheysa'
import Phelkay from './Phelkay'
import { MaterialIcons } from '@expo/vector-icons'
import { QuizHomeScreen } from './QuizHomeScreen'
import AnimalQuizData from './AnimalQuizData'
import ColorQuiz from './ColorQuiz'
import { EvilIcons } from '@expo/vector-icons';
import { Provider } from 'react-native-paper'
const HomeScreen = ({navigation}) => {
  return (
    
    <Provider >
      
      <View style={styles.head}>
   
        <MaterialIcons name="sort" size={34} style={{marginTop:40, marginLeft:10}} color='black' onPress={navigation.toggleDrawer}/>
        {/* <EvilIcons name="navicon" size={30} style={{marginTop:40, marginLeft:10}} color="black" onPress={navigation.toggleDrawer}/> */}
        <Text style={styles.Header}>གུས་ཞབས།</Text>
      </View>
      <ScrollView style={{backgroundColor:'#46caeb',}}>
    <View style={styles.container}>
    <Text style={{fontWeight:'bold',fontSize:40,textAlign:'center',alignItems:'center', }}>གདམ་ཁ་མཛད་གནང།</Text>
      <TouchableOpacity 
      onPress={()=>{
        navigation.navigate("zheysa")
      }}
      
      style={styles.content}>
        <Image
        style={styles.imageIc}
        source={require('../../assets/respect.png')}

        />
        <Text style={{textAlign:'center',fontWeight:'bold',fontSize:20,padding:10}}>ཞེ་ས་ལས་ཕལ་སྐད་སྒྱུར་།</Text>
      </TouchableOpacity>
        
      <TouchableOpacity  
       onPress={() => {
         navigation.navigate('Phelkay')
       }}
      style={styles.content}>
        <Image
        style={styles.imageIc}
        source={require('../../assets/friendship.png')}
        />
        <Text style={{textAlign:'center',fontWeight:'bold',fontSize:20,padding:10}}>ཕལ་སྐད་ལས་ཞེ་ས་སྐད་སྒྱུར་།</Text>
      </TouchableOpacity>

      <TouchableOpacity
       style={styles.content}
       
       onPress={()=>{
        navigation.navigate("Quiz")
      }}
      >
          <Image
        style={styles.imageIc}
        source={require('../../assets/quiz.png')}
        />
        <Text style={{textAlign:'center',fontWeight:'bold',fontSize:20,padding:10}}>རིག་བསྡུར།</Text>
      </TouchableOpacity>
    </View>
    </ScrollView>
    </Provider>
   
  )
}

export default HomeScreen

const styles = StyleSheet.create({
  head:{

    backgroundColor:'#46caeb',
  
  },
  Header:{
    fontSize:40,
    paddingLeft:120,
    fontWeight:'bold',
    marginTop:30
    
    
  },
 

    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        // marginTop:-500,
        backgroundColor:'white',
        borderTopRightRadius:50,
        borderTopLeftRadius:50,
        paddingBottom:60
      
        
       
        
    },
    content:{
      borderWidth:2,
      borderColor:'black',
      marginVertical:20,
      borderRadius:10,
      backgroundColor:'#46caeb',
      
    },
    imageIc:{
    width:250,
    height:100,
    resizeMode:'contain',
    opacity:0.98
    }
})
