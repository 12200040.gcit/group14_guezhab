import { StyleSheet, Text, View,Image} from 'react-native'
import React from 'react'
import { NavigationContainer} from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { Provider,Button}from 'react-native-paper';
import { HomeScreen, Quiz } from './src/Screen';
import { Zheysa } from './src/Screen';
import { Phelkay, PhelkayContent } from './src/Screen';
import { Handresult } from './src/Screen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from './src/components/DrawerContent';
import { Cha } from './src/Screen';
import { AnimalQuiz } from './src/Screen';
import { ColorQuiz } from './src/Screen';
import Aboutus from './src/Screen/Aboutus';



import { FavouriteScreen } from './src/Screen';
import { FontAwesome5 } from '@expo/vector-icons';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App({navigation}){

  return(
  
    <Provider>
   
        <NavigationContainer
        initialRouteName='DrawerNavigator'

        >
          <Stack.Navigator
            screenOptions={{
              headerShown:false
            }}
          >
            <Stack.Screen name='home' component={DrawerNavigator}/>
            <Stack.Screen name='zheysa' component={Zheysa}/>
            <Stack.Screen name="Phelkay" component={Phelkay}/>
            <Stack.Screen name="Favourite" component={FavouriteScreen}/>
            <Stack.Screen name="PhelkayContent" component={PhelkayContent}/>
            <Stack.Screen name='handresult' component={Handresult}/>
            <Stack.Screen name='cha' component={Cha}/>
            <Stack.Screen name="Quiz" component={Quiz}/>
            <Stack.Screen name='animalquize' component={ AnimalQuiz}/>
            <Stack.Screen name='colorquize' component={ColorQuiz}/>
            <Stack.Screen name='Aboutus' component={Aboutus}/>

          </Stack.Navigator>
        </NavigationContainer>
  
   
    </Provider>
  )
}

function BottomNavigation({navigation}){
  return(
    <Tab.Navigator screenOptions={{
      headerShown:false
    }}>

      
      <Tab.Screen 
      options={{
        tabBarIcon:({size})=>{
          return(
    
            <FontAwesome5 name="home" size={24} color="black" />
          )
        }
      }}
      
      name="Home" 
      component={HomeScreen}/>

      <Tab.Screen
       options={{
        tabBarIcon:({size})=>{
          return(
            <Image
            style={{width:40,height:40}}
            source={
              require('./assets/respect.png')
            }
            />
          )
        }
      }}
      name="Zheysa" component={Zheysa}/>

      <Tab.Screen
       options={{
        tabBarIcon:({size})=>{
          return(
            <Image
            style={{width:40,height:38}}
            source={
              require('./assets/friendship.png')
            }
            />
          )
        }
      }}
      name="Phelkay" component={Phelkay}/>
      
    </Tab.Navigator>
    
  )
}

const DrawerNavigator = ({navigation}) => {
  return(
    <Drawer.Navigator drawerContent={DrawerContent}
   screenOptions={{headerShown:false}}>
      <Drawer.Screen
      name='Guezhab' component={BottomNavigation}/>

    </Drawer.Navigator>
  )
} 

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  }
})





